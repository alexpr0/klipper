#ifndef __EMUL_INTERNAL_H
#define __EMUL_INTERNAL_H

#include <signal.h> // sigset_t
#include <stdint.h> // uint32_t
#include "autoconf.h"

#define NSECS 1000000000
#define NSECS_PER_TICK (NSECS / CONFIG_CLOCK_FREQ)

// gpio.c
int gpio_setup(char *config_string);

// console.c
void report_errno(char *where, int rc);
int set_non_blocking(int fd);
int set_close_on_exec(int fd);
int console_setup(char *name);
void console_sleep(sigset_t *sigset);

// timer.c
int timer_check_periodic(uint32_t *ts);
void timer_disable_signals(void);
void timer_enable_signals(void);

#endif // internal.h
