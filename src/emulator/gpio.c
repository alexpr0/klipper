#include <libmemcached/memcached.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "command.h"
#include "internal.h"
#include "board/gpio.h"

DECL_CONSTANT("ADC_MAX", CONFIG_EMU_ADS_MAX);

memcached_st *memc;
char gpio_key_buf[64];
char gpio_val_buf[128];
const char *gpio_in_fmt = "GPIO:IN:%u";
const char *gpio_out_fmt = "GPIO:OUT:%u";
const char *gpio_pwm_fmt = "GPIO:PWM:%u";
const char *gpio_adc_fmt = "GPIO:ADC:%u";

int gpio_setup(char *config_string) {
    memc = memcached(config_string, strlen(config_string));
    return memc == NULL ? -1 : 0;
}

uint32_t gpio_read(uint32_t pin, const char *fmt) {
    snprintf(gpio_key_buf, sizeof(gpio_key_buf), fmt, pin);

    memcached_return_t rc;
    char *buf;
    size_t len;
    uint32_t flags;
    uint32_t val = 0;

    buf = memcached_get(memc, gpio_key_buf, strlen(gpio_key_buf), &len, &flags, &rc);
    if(rc == MEMCACHED_SUCCESS) {
        if (len >= sizeof(gpio_val_buf)) {
            rc = MEMCACHED_UNKNOWN_READ_FAILURE;
        }
        else {
            memcpy(gpio_val_buf, buf, len);
            gpio_val_buf[len] = 0;
            val = atol(gpio_val_buf);
        }
        free(buf);
    }
    if(rc != MEMCACHED_SUCCESS) {
        snprintf(gpio_val_buf, sizeof(gpio_val_buf), "%u", 0);
        memcached_set(memc, gpio_key_buf, strlen(gpio_key_buf), gpio_val_buf, strlen(gpio_val_buf), (time_t)0, (uint32_t)0);
    }

    return val;
}
void gpio_write(uint32_t pin, uint32_t val, const char *fmt) {
    snprintf(gpio_key_buf, sizeof(gpio_key_buf), fmt, pin);
    snprintf(gpio_val_buf, sizeof(gpio_val_buf), "%u", val);
    memcached_set(memc, gpio_key_buf, strlen(gpio_key_buf), gpio_val_buf, strlen(gpio_val_buf), (time_t)0, (uint32_t)0);
}

// --- OUT ---
struct gpio_out gpio_out_setup(uint32_t pin, uint32_t val) {
    gpio_write(pin, val, gpio_out_fmt);
    return (struct gpio_out){.pin=pin};
}
void gpio_out_reset(struct gpio_out g, uint32_t val) {
    gpio_write(g.pin, val, gpio_out_fmt);
}
void gpio_out_write(struct gpio_out g, uint32_t val) {
    gpio_write(g.pin, val, gpio_out_fmt);
}
void gpio_out_toggle_noirq(struct gpio_out g) {
    uint32_t val = gpio_read(g.pin, gpio_out_fmt);
    val = val > 0 ? 0 : 1;
    gpio_write(g.pin, val, gpio_out_fmt);
}
void gpio_out_toggle(struct gpio_out g) {
    gpio_out_toggle_noirq(g);
}

// --- IN ---
struct gpio_in gpio_in_setup(uint32_t pin, int32_t pull_up) {
    gpio_write(pin, pull_up > 0 ? pull_up : 0, gpio_in_fmt);
    return (struct gpio_in){.pin=pin};
}
void gpio_in_reset(struct gpio_in g, int32_t pull_up) {
    gpio_write(g.pin, pull_up > 0 ? pull_up : 0, gpio_in_fmt);
}
uint32_t gpio_in_read(struct gpio_in g) {
    return gpio_read(g.pin, gpio_in_fmt);
}

// --- PWM ---
struct gpio_pwm gpio_pwm_setup(uint32_t pin, uint32_t cycle_time, uint32_t val) {
    struct gpio_pwm g = {0};
    g.pin = pin;
    g.cycle_time = cycle_time;
    gpio_pwm_write(g, val);
    return g;
}
void gpio_pwm_write(struct gpio_pwm g, uint32_t val) {
    snprintf(gpio_key_buf, sizeof(gpio_key_buf), gpio_pwm_fmt, g.pin);
    snprintf(gpio_val_buf, sizeof(gpio_val_buf), "%u/%u", val, g.cycle_time);
    memcached_set(memc, gpio_key_buf, strlen(gpio_key_buf), gpio_val_buf, strlen(gpio_val_buf), (time_t)0, (uint32_t)0);
}

// --- ADC ---
struct gpio_adc gpio_adc_setup(uint32_t pin) {
    gpio_write(pin, (CONFIG_EMU_ADS_MAX + 1) / 2, gpio_adc_fmt);
    return (struct gpio_adc){.pin=pin};
}
uint32_t gpio_adc_sample(struct gpio_adc g) {
    return 0;
}
uint32_t gpio_adc_read(struct gpio_adc g) {
    return gpio_read(g.pin, gpio_adc_fmt);
}
void gpio_adc_cancel_sample(struct gpio_adc g) {
}

// --- SPI ---
struct spi_config spi_setup(uint32_t bus, uint8_t mode, uint32_t rate) {
    return (struct spi_config){};
}
void spi_prepare(struct spi_config config) {
}
void spi_transfer(struct spi_config config, uint8_t receive_data, uint8_t len, uint8_t *data) {
}
